package fr.epsi.rennes.mspr.tpre814.shared.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import fr.epsi.rennes.mspr.tpre814.shared.events.BaseEvent;

import java.io.Serial;
import java.io.Serializable;
import java.util.UUID;

public final class Customer implements BaseEvent, Serializable {
    private static final @Serial long serialVersionUID = -1840553357363193935L;
    UUID id;
    String username;
    UserProfile profile;
    Address address;
    Company company;

    public Customer() {
        // default constructor for Jackson
    }

    @JsonCreator
    public Customer(@JsonProperty("id") UUID id,
                    @JsonProperty("username") String username,
                    @JsonProperty("profile") UserProfile profile,
                    @JsonProperty("address") Address address,
                    @JsonProperty("company") Company company) {
        this.id = id;
        this.username = username;
        this.profile = profile;
        this.address = address;
        this.company = company;
    }

    public Customer(UUID customerId) {
        this.id = customerId;
        this.profile = new UserProfile();
        this.address = new Address();
        this.company = new Company();
    }

    @Override
    public String toJson() {
        return "{" +
                "\"id\":\"" + id + "\"," +
                "\"username\":\"" + username + "\"," +
                "\"profile\":{" + getProfile().toJson() + "}," +
                "\"address\":{" + getAddress().toJson() + "}," +
                "\"company\":{" + getCompany().toJson() + "}" +
                "}";
    }

    public UserProfile getProfile() {
        if (profile == null) {
            profile = new UserProfile();
        }
        return profile;
    }

    public Address getAddress() {
        if (address == null) {
            address = new Address();
        }
        return address;
    }
    public Company getCompany() {
        if (company == null) {
            company = new Company();
        }
        return company;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setProfile(UserProfile profile) {
        this.profile = profile;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public void setCompany(Company company) {
        this.company = company;
    }


    @Override
    public String toString() {
        return toJson();
    }
}
