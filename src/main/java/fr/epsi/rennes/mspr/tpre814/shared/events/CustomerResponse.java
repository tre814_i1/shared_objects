package fr.epsi.rennes.mspr.tpre814.shared.events;

import fr.epsi.rennes.mspr.tpre814.shared.models.Customer;

import java.io.Serial;
import java.io.Serializable;
import java.util.UUID;

/**
 * Payload is a Customer object
 * This event is sent by the Customer Service to any Service that requested Customer information
 */
public class CustomerResponse extends AbstractEvent<Customer> implements Serializable {
    @Serial
    private static final long serialVersionUID = -8533881204454573189L;

    public CustomerResponse() { // Default constructor, must be present for Serialization/Deserialization
        super();
    }

    public CustomerResponse(UUID uuid) {
        super(uuid);
    }

    @Override
    public String toJson() {
        return "{\"id\":\"" + getId() + "\", \"payload\": " + getPayload().toJson() + "}";
    }

    @Override
    public String toString() {
        return String.format("CustomerRequest{id=%s}", getId());
    }
}
