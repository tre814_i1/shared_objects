package fr.epsi.rennes.mspr.tpre814.shared.utils;

import fr.epsi.rennes.mspr.tpre814.shared.events.ProductRequest;
import fr.epsi.rennes.mspr.tpre814.shared.models.Product;

import java.util.List;

public class ItemMapper implements ModelMapper {

    private ItemMapper() {
        // Prevent instantiation
    }

    public static ProductRequest toProductRequest(Product product) {
        return new ProductRequest(product.getId(), product.getQuantity());
    }

    public static List<ProductRequest> toProductRequest(List<Product> products) {
        return products.stream()
                .map(ItemMapper::toProductRequest)
                .toList();
    }

    public static Product toProduct(ProductRequest request) {
        Product product = new Product(request.id());
        product.setQuantity(request.quantity());
        return product;
    }

    public static List<Product> toProduct(List<ProductRequest> requests) {
        return requests.stream()
                .map(ItemMapper::toProduct)
                .toList();
    }

}
