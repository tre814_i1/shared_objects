package fr.epsi.rennes.mspr.tpre814.shared.events;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serial;
import java.io.Serializable;
import java.util.UUID;

/**
 * This is an __Event Payload__ that represents a request for a product.
 * It contains the product's ID and the quantity requested.
 */
public record ProductRequest(UUID id, int quantity) implements Serializable {
    @Serial
    private static final long serialVersionUID = 3140185196648766324L;

    @JsonCreator
    public ProductRequest(@JsonProperty("id") UUID id, @JsonProperty("quantity") int quantity) {
        this.id = id;
        this.quantity = quantity;
    }

    public String toJson() {
        return "{\"id\":\"" + id + "\",\"quantity\":\"" + quantity + "\"}";
    }
}
