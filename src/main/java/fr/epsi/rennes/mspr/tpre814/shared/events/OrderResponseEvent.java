package fr.epsi.rennes.mspr.tpre814.shared.events;

import fr.epsi.rennes.mspr.tpre814.shared.models.OrderDTO;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

/**
 * This is sent by any service that has the means to create operations on the stock.
 * It contains a list of {@link ProductRequest} objects as a Payload and a {@link StockOperation}) as an extra argument.
 */
public class OrderResponseEvent extends AbstractEvent<List<OrderDTO>> implements Serializable {
    @Serial
    private static final long serialVersionUID = 4867755366523820577L;


    public OrderResponseEvent() {
        super();
    }

    public OrderResponseEvent(UUID id, List<OrderDTO> payload) {
        super(id);
        this.setPayload(payload);
    }


    @Override
    public String toString() {
        return "{" +
                "id=" + getId() +
                ", payload=" + getPayload() +
                '}';
    }

    @Override
    public String toJson() {
        return "{" +
                "\"id\":\"" + getId() +
                "\", \"payload\": [" +
                getPayload().stream()
                        .map(OrderDTO::toJson)
                        .reduce((a, b) -> a + "," + b)
                        .orElse("") +
                "]}";
    }

}