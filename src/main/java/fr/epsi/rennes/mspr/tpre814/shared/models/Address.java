package fr.epsi.rennes.mspr.tpre814.shared.models;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serial;
import java.io.Serializable;
import java.util.UUID;


public class Address implements Serializable {
    private static final @Serial long serialVersionUID = 8952948057370517257L;
    UUID id;
    String street;
    String city;
    String zipcode;
    String country;

    public Address() {
        // default constructor for Jackson
    }

    public Address(UUID id) {
        this.id = id;
    }

    @JsonCreator
    public Address(@JsonProperty("id") UUID id,
                   @JsonProperty("street") String street,
                   @JsonProperty("city") String city,
                   @JsonProperty("zipcode") String zipcode,
                   @JsonProperty("country") String country) {
        this.id = id;
        this.street = street;
        this.city = city;
        this.zipcode = zipcode;
        this.country = country;
    }

    public Address(String id) {
        this.id = UUID.fromString(id);
    }

    @Override
    public String toString() {
        return String.format("Address{street='%s', city='%s', zipcode='%s', country='%s'}", street, city, zipcode, country);
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String toJson() {
        return "\"id\":\"" + id + "\"," +
                "\"street\":\"" + street + "\"," +
                "\"city\":\"" + city + "\"," +
                "\"zipcode\":\"" + zipcode + "\"," +
                "\"country\":\"" + country + "\"";
    }
}
