package fr.epsi.rennes.mspr.tpre814.shared.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serial;
import java.io.Serializable;
import java.util.UUID;

public class Company implements Serializable {
    private static final @Serial long serialVersionUID = -1948008656821597303L;
    UUID id;
    String name;
    String siret;
    Address address;

    public Company() {
        // default constructor for Jackson
    }

    @JsonCreator
    public Company(
            @JsonProperty("id") UUID id,
            @JsonProperty("name") String name,
            @JsonProperty("siret") String siret,
            @JsonProperty("address") Address address
    ) {
        this.id = id;
        this.name = name;
        this.siret = siret;
        this.address = address;
    }

    @Override
    public String toString() {
        return String.format("Company{name='%s', siret='%s', address=%s}", name, siret, address);
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSiret() {
        return siret;
    }

    public void setSiret(String siret) {
        this.siret = siret;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String toJson() {
        return "\"id\":\"" + id + "\"," +
                "\"name\":\"" + name + "\"," +
                "\"siret\":\"" + siret + "\"," +
                "\"address\":{" + (address == null ? "" : address.toJson()) + "}";
    }
}
