package fr.epsi.rennes.mspr.tpre814.shared.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serial;
import java.io.Serializable;
import java.util.UUID;

public class Product implements Serializable {
    private static final @Serial long serialVersionUID = -5547532005087988719L;
    UUID id;
    int quantity;
    String name;
    ProductDetails details;
    int stock;

    public Product(@JsonProperty("id") UUID id) {
        this.id = id;
        this.details = new ProductDetails();
    }

    public Product() {
        // default constructor for Jackson
        this.details = new ProductDetails();
    }

    @Override
    public String toString() {
        return toJson();
    }

    public String toJson() {
        return "{\"id\":\"" + id + "\"," +
                "\"name\":\"" + name + "\"," +
                "\"details\":{" + (details == null ? "" : details.toJson()) + "}," +
                "\"stock\":\"" + stock + "\"}";
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProductDetails getDetails() {
        return details;
    }

    public void setDetails(ProductDetails details) {
        this.details = details;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }


    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + quantity;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (details != null ? details.hashCode() : 0);
        result = 31 * result + stock;
        return result;
    }

}
