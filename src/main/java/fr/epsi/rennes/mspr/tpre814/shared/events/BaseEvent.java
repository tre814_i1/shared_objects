package fr.epsi.rennes.mspr.tpre814.shared.events;


import java.io.Serializable;

/**
 * Defines the methods that an Event must implement.
 */
public interface BaseEvent extends Serializable {

    String toString();

    String toJson();
}
