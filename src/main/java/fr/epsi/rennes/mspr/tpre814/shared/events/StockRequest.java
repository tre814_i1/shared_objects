package fr.epsi.rennes.mspr.tpre814.shared.events;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

/**
 * This is sent by any service that has the means to create operations on the stock.
 * It contains a list of {@link ProductRequest} objects as a Payload and a {@link StockOperation}) as an extra argument.
 */
public class StockRequest extends AbstractEvent<List<ProductRequest>> implements Serializable {
    @Serial
    private static final long serialVersionUID = 4867755366523820577L;

    private StockOperation op;

    public StockRequest() {
        super();
    }

    public StockRequest(UUID id, List<ProductRequest> payload) {
        super(id);
        this.setPayload(payload);
    }

    public StockOperation getOp() {
        return op;
    }

    public void setOp(StockOperation op) {
        if (op == null) {
            throw new IllegalArgumentException("op cannot be null");
        }
        this.op = op;
    }

    @Override
    public String toString() {
        return "{" +
                "id=" + getId() +
                ", payload=" + getPayload() +
                ", op=" + op +
                '}';
    }

    @Override
    public String toJson() {
        return "{" +
                "\"id\":\"" + getId() +
                "\", \"payload\": [" +
                getPayload().stream()
                        .map(ProductRequest::toJson)
                        .reduce((a, b) -> a + "," + b)
                        .orElse("") +
                "], \"op\":\"" + op + "\"}";
    }

}