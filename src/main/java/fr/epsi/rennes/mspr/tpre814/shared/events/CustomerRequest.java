package fr.epsi.rennes.mspr.tpre814.shared.events;


import java.io.Serial;
import java.io.Serializable;
import java.util.UUID;

/**
 * Payload is the customer ID
 * This event is sent by any Service that needs Customer information to the Customer Service
 */
public class CustomerRequest extends AbstractEvent<UUID> implements Serializable {
    @Serial
    private static final long serialVersionUID = -8533881204454573189L;
    private UUID orderId;

    public CustomerRequest() { // Default constructor, must be present for Serialization/Deserialization
        super();
    }

    public CustomerRequest(UUID key, UUID orderId) {
        super(key);
        setOrderId(orderId);
    }

    public UUID getOrderId() {
        return orderId;
    }

    public void setOrderId(UUID orderId) {
        if (orderId == null) {
            throw new IllegalArgumentException("orderId cannot be null");
        }
        this.orderId = orderId;
    }

    @Override
    public String toJson() {
        return "{" +
                "\"id\":\"" + getId() + "\"," +
                "\"orderId\":\"" + orderId + "\"" +
                "}";
    }

    @Override
    public String toString() {
        return String.format("CustomerRequest{id=%s, orderId=%s}", getId(), orderId);
    }
}
