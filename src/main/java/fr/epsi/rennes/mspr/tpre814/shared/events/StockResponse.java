package fr.epsi.rennes.mspr.tpre814.shared.events;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import fr.epsi.rennes.mspr.tpre814.shared.models.Product;

import java.io.Serial;
import java.util.List;
import java.util.UUID;

/**
 * This Event is sent by the Stock Service in response to a {@link StockRequest}.
 * It contains the list of requested products as Payload.
 */
public class StockResponse extends AbstractEvent<List<Product>> {
    @Serial
    private static final long serialVersionUID = -5249223527726856062L;

    @JsonCreator
    public StockResponse(@JsonProperty("id") UUID id, @JsonProperty("products") List<Product> products) {
        this.setId(id);
        this.setPayload(products);
    }

    @Override
    public String toJson() {
        return "{ \"id\":\"" + getId() +
                "\", \"products\": [" +
                getPayload().stream()
                        .map(Product::toJson)
                        .reduce((a, b) -> a + "," + b)
                        .orElse("") +
                "]}";
    }
}
