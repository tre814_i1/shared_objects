package fr.epsi.rennes.mspr.tpre814.shared.enums;

public enum OrderStatus {
    INIT, // Order created
    RESERVED,  // Stock reserved
    PARTIAL, // Partially confirmed (some products are not available)
    CONFIRMED, // Customer confirmed
    READY, // Validations are done
    // ...
    CANCELLED, CLOSED, ARCHIVED, UNKNOWN;
}