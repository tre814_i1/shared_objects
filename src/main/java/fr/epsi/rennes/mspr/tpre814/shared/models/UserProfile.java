package fr.epsi.rennes.mspr.tpre814.shared.models;

import java.io.Serial;
import java.io.Serializable;
import java.util.UUID;

import static java.time.LocalTime.now;

public final class UserProfile implements Serializable {

    @Serial
    private static final long serialVersionUID = 8457396466739115897L;
    UUID id;
    String createdAt;
    String firstName;
    String lastName;

    public UserProfile() {
        // default constructor for Jackson
    }

    public UserProfile(UUID id) {
        this.id = id;
    }

    public UserProfile(UUID id, String firstName, String lastName) {
        this.id = id;
        this.createdAt = String.valueOf(now());
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return toJson();
    }

    public String toJson() {
        return "\"id\":\"" + id + "\"," +
                "\"createdAt\":\"" + createdAt + "\"," +
                "\"firstName\":\"" + firstName + "\"," +
                "\"lastName\":\"" + lastName + "\"";
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
