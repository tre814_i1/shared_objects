package fr.epsi.rennes.mspr.tpre814.shared.models;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import fr.epsi.rennes.mspr.tpre814.shared.enums.OrderStatus;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


public class OrderDTO implements Serializable {
    @Serial
    private static final long serialVersionUID = 4430030793158364370L;
    UUID id;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    LocalDateTime createdAt;
    OrderStatus status;
    UUID customerId;
    double totalPrice;
    UUID addressId;
    String warn;
    private List<Product> products;

    public OrderDTO() {
        // default
    }

    @JsonCreator
    public OrderDTO orderDTO(@JsonProperty("id") UUID id,
                             @JsonProperty("createdAt") LocalDateTime createdAt,
                             @JsonProperty("status") OrderStatus status,
                             @JsonProperty("customerId") UUID customerId,
                             @JsonProperty("totalPrice") double totalPrice,
                             @JsonProperty("products") List<Product> products,
                             @JsonProperty("addressId") UUID addressId,
                             @JsonProperty("warn") String warn) {
        this.id = id;
        this.createdAt = createdAt;
        this.status = status;
        this.customerId = customerId;
        this.totalPrice = totalPrice;
        this.products = products;
        this.addressId = addressId;
        this.warn = warn;
        return this;
    }

    @Override
    public String toString() {
        return toJson();
    }

    public String toJson() {
        return "{" +
                "\"id\":\"" + id + "\"," +
                "\"createdAt\":\"" + createdAt + "\"," +
                "\"status\":\"" + status + "\"," +
                "\"customerId\":\"" + customerId + "\"," +
                "\"totalPrice\":\"" + totalPrice + "\"," +
                "\"products \":[" + products + "]," +
                "\"addressId\":\"" + addressId + "\"," +
                "\"warn\":\"" + warn + "\"" +
                "}";
    }

    public void addProduct(Product product) {
        products.add(product);
    }

    public void removeProduct(Product product) {
        products.remove(product);
    }

    public void clearProducts() {
        products.clear();
    }

    public List<Product> getProducts() {
        if (products == null) {
            products = new ArrayList<>();
        }
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public String getWarn() {
        return warn;
    }

    public void setWarn(String warn) {
        this.warn = warn;
    }

    public UUID getAddressId() {
        return addressId;
    }

    public void setAddressId(UUID addressId) {
        this.addressId = addressId;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public UUID getCustomerId() {
        return customerId;
    }

    public void setCustomerId(UUID customerId) {
        this.customerId = customerId;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }
}
