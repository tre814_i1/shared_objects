package fr.epsi.rennes.mspr.tpre814.shared.events;

import java.io.Serial;
import java.util.Objects;
import java.util.UUID;

/**
 * Abstract class for all events.
 * Default structure : {
 * eventId: UUID,
 * payload: T
 * }
 *
 * @param <T> The payload type
 */
public abstract class AbstractEvent<T> implements BaseEvent {
    @Serial
    private static final long serialVersionUID = -2953212693793443384L;
    private UUID eventId;
    private transient T payload;

    AbstractEvent() {
        // Default constructor, must be present for Serialization/Deserialization
    }

    AbstractEvent(UUID eventId) {
        this();
        this.eventId = eventId;
    }

    @Override
    public String toString() {
        return String.format("{\"id\"=\"%s\", \"payload\"=%s}", eventId, payload);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AbstractEvent<?> that = (AbstractEvent<?>) o;

        if (!Objects.equals(eventId, that.eventId)) return false;
        return Objects.equals(payload, that.payload);
    }

    @Override
    public int hashCode() {
        int result = eventId != null ? eventId.hashCode() : 0;
        result = 31 * result + (payload != null ? payload.hashCode() : 0);
        return result;
    }

    public UUID getId() {
        return this.eventId;
    }

    public void setId(UUID id) {
        this.eventId = id;
    }

    public T getPayload() {
        return this.payload;
    }

    public void setPayload(T payload) {
        this.payload = payload;
    }
}
