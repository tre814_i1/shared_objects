package fr.epsi.rennes.mspr.tpre814.shared.models;


import java.io.Serial;
import java.io.Serializable;

public class ProductDetails implements Serializable {
    private static final @Serial long serialVersionUID = -1379236635239762853L;
    String name;
    String description;
    double price;

    public ProductDetails() {
        // default constructor for Jackson
    }

    public ProductDetails(String name, String description, double price) {
        this.name = name;
        this.description = description;
        this.price = price;
    }

    @Override
    public String toString() {
        return toJson();
    }

    public String toJson() {
        return "\"name\":\"" + name + "\"," +
                "\"description\":\"" + description + "\"," +
                "\"price\":\"" + price + "\"";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
