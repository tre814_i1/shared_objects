package fr.epsi.rennes.mspr.tpre814.shared.events;

/**
 * {@link StockRequest} Related !
 */
public enum StockOperation {
    SUB,
    ADD,
}
