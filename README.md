# Application multi-services : Order-Manager

Auteur :
Mistayan - [Gitlab](https://gitlab.com/Mistayan/)

## Description

Définition des objets relatifs aux échanges inter-services

## Licence

Ce projet est sous licence MIT - voir le fichier [LICENSE.md](LICENSE.md) pour plus d'informations

## Contacts

Mistayan - [Gitlab](https://gitlab.com/Mistayan/) 
Bob-117 - [Gitlab](https://gitlab.com/Bob-117/)
Sullfurick - [Gitlab](https://gitlab.com/Sullfurick)