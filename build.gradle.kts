plugins {
    id("java")
    id("application")
    id("maven-publish")
}
apply(plugin = "maven-publish")
group = "fr.epsi.rennes.mspr.tpre814"
version = "0.0.6"

java {
    sourceCompatibility = JavaVersion.VERSION_21
}

configurations {
    compileOnly {
        extendsFrom(configurations.annotationProcessor.get())
    }
}

repositories {
    mavenCentral()
    gradlePluginPortal()
}

dependencies {
    //jackson dependencies
    implementation("com.fasterxml.jackson.core:jackson-databind:2.17.1")
}
tasks.withType<JavaCompile> {
    options.encoding = "UTF-8"
    options.compilerArgs.addAll(listOf("-parameters"))
}

tasks.jar {
    manifest {
        attributes(mapOf("Implementation-Title" to project.name,
            "Implementation-Version" to project.version))
    }
}

// reserved for CI use only
publishing {
    publications {
        create<MavenPublication>("library") {
            from(components["java"])
        }
    }
    repositories {
        maven {
            // 58301042 : shared-objects : https://gitlab.com/tre814_i1/shared_objects/
            url = uri("https://gitlab.com/api/v4/projects/${System.getenv("CI_PROJECT_ID")}/packages/maven")
            name = "Gitlab"
            credentials {
                username = "gitlab-ci-token"
                password = System.getenv("CI_JOB_TOKEN")
            }
        }
    }
}